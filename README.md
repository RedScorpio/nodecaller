# nodecaller

Allows to easily chain calls to server and test rest APIs.

## Sample Usage

    function done(err){
      console.log(err? ('An error occurred: ' + err) : 'Call successful');
    }
    
    var caller = new Caller('http://localhost.com:3000/api/');
    caller
      .path('ok')
      .get() //Expects 200, no other checks performed
      .path('auth/login')
      .post({ 'user': 'username', 'pass': 'password123' }, function(result, next){
        //Do something with result.body, remember to call next at the end
      }) //Expects 200, will pass an object as a json request body, will call function on success
      .put(function(result, next){
        //Do something with 404 result, remember to call next at the end
      }, 404) //Sends empty PUT request to http://localhost.com:3000/api/auth/login and expects 404
      .run(done);
      


TODO :)