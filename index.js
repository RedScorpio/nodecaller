'use strict';

var async = require('async'),
  request = require('request'),
  supertest = require('supertest'),
  chalk = require('chalk'),
  path = require('path'),
  fs = require('fs'),
  urlJoin = require('url-join');

chalk.enabled = true;
chalk.supportsColor = true;

var FileUpload = function (pathToFile) {
  this.path = path.resolve(pathToFile);
};

/**
 * @param {string|Express} callPath - Main path.
 * @constructor
 * @property {Array} calls - Call chain.
 * @property {string} subPath - Second part of call url.
 * @property {object} globals - Object with preferences shared across.
 * @property {request.Request} agent
 */
var Caller = function (callPath, app) {
  if (app) {
    if (!callPath || !callPath.match(/\/.*/)) {
      throw new Error('Call path "' + callPath + '" is not a valid URL ending.');
    }
    this.request = supertest(app);
  } else if (!callPath || !callPath.match(/.*?:\/\/.*?\/.*/)) {
    throw new Error('Call path "' + callPath + '" is not a valid URL.');
  }
  this.calls = [];
  this.apiPath = callPath;
  this.subPath = '';
  this.jsonFormat = true;
  this.errorContinuation = false;
  this._reset();
};

Caller.FileUpload = FileUpload;

Caller.prototype.setApiPath = function (path) {
  this.apiPath = path;
  return this;
};
Caller.prototype.headers = function (headerValues) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    if (!headerValues) {
      return done('No value object have been passed to headers call.');
    }
    if (typeof headerValues === 'function') {
      $.headersObject = headerValues(value);
    } else {
      $.headersObject = headerValues;
    }
    done(null, value);
  });
  return $;
};
Caller.prototype.then = function (func) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    func(value, done);
  });
  return $;
};

var queryMethods = ['GET', 'DELETE'];
var requestColors = {
  GET: chalk.green,
  PUT: chalk.yellow,
  POST: chalk.blue,
  DELETE: chalk.red
};

Caller.prototype._request = function (method, params, callback) {
  var $ = this;
  if (typeof params.query === 'function') {
    params.query = params.query(params.value);
  }
  if (this.request) {
    let req, url = urlJoin($.apiPath, $.subPath);
    switch (method) {
      case 'GET':
        req = this.request.get(url).query(params.query);
        break;
      case 'POST':
        req = this.request.post(url).send(params.query);
        break;
      case 'PUT':
        req = this.request.put(url).send(params.query);
        break;
      case 'DELETE':
        req = this.request.delete(url).query(params.query);
        break;
    }
    if ($.headersObject) {
      req.set($.headersObject);
    }
    if (params.multipart) {
      for (let field in params.query) {
        if (params.query.hasOwnProperty(field)) {
          let fieldValue = params.query[field];
          if (fieldValue instanceof FileUpload) {
            req.attach(field, fieldValue.path);
          } else {
            req.field(field, fieldValue);
          }
        }
      }
    }
    if ($.debugging) {
      let color = requestColors[method] || chalk.white;
      console.log(color.inverse.bold(method) + ' ' + color(url));
      console.log(JSON.stringify(params));
    }
    req.expect(params.code)
      .end((err, response) => {
        if (err) {
          try {
            err.message += '\r\n' + JSON.stringify(response.body);
          } catch (e) {
          }
        }
        if (!response) {
          return callback(new Error('Response not received.'));
        }
        let res = response.body;
        if (params.code !== response.statusCode) {
          if (typeof res !== 'string') {
            res = JSON.stringify(response.body, 0, 2);
          }
          err = new Error(url + '\nExpected status code to equal ' + params.code + ', got ' + response.statusCode + '. Response:\n' + res);
        }
        return callback(err, response, res);
      });
  } else {
    let options = {
      method: method,
      uri: urlJoin($.apiPath, $.subPath),
      headers: $.headersObject
    };
    if (typeof params.query === 'function') {
      params.query = params.query(params.value);
    }
    if (queryMethods.indexOf(method) >= 0) {
      options.qs = params.query;
    }
    if ($.jsonFormat) {
      options.json = true;
    }
    if (params.multipart) {
      let multipart = {};
      for (let field in params.query) {
        if (params.query.hasOwnProperty(field)) {
          let fieldValue = params.query[field];
          if (fieldValue instanceof FileUpload) {
            multipart[field] = fs.createReadStream(fieldValue.path);
          } else {
            multipart[field] = fieldValue;
          }
        }
      }
      options.formData = multipart;
    } else {
      options.body = params.query;
    }
    if ($.debugging) {
      let color = requestColors[method] || chalk.white;
      console.log(color.inverse.bold(method) + ' ' + color(options.uri));
      console.log(JSON.stringify(params));
    }
    return request(options, function (err, response, body) {
      if (err) {
        try {
          error.message += '\r\n' + JSON.stringify(response.body);
        } catch (e) {
        }
      }
      if (!response) {
        err = new Error('Response not received.');
      } else if (params.code !== response.statusCode) {
        let res = response.body;
        if (typeof res !== 'string') {
          res = JSON.stringify(response.body, 0, 2);
        }
        err = new Error('Expected status code to equal ' + params.code + ', got ' + response.statusCode + '. Response:\n' + res);
      }
      return callback(err, response, body);
    });
  }
};

Caller.prototype.debug = function (enable) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    $.debugging = (enable === undefined) || (enable === true);
    done(null, value);
  });
  return $;
};
Caller.prototype._flag = function (name, flag) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    $[name] = (flag === undefined) || (flag === true);
    done(null, value);
  });
  return $;
};
Caller.prototype.raw = function (enable) {
  return this._flag('rawResponse', enable);
};
Caller.prototype.wait = function (milliseconds) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    setTimeout(function () {
      done(null, value);
    }, milliseconds);
  });
  return $;
};
Caller.prototype.path = function (path) {
  var $ = this;
  $.calls.push(function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    if (typeof path === 'function') {
      $.subPath = path(value);
    } else {
      $.subPath = path;
    }
    done(null, value);
  });
  return $;
};

var callbackStub = function (result, next) {
  next(null, result.value);
};

Caller.prototype.prepareParams = function (query, callback, code) {
  //Parse arguments
  if (typeof callback === 'number') {
    code = callback;
    callback = undefined;
  }
  if (typeof query === 'function') {
    if (query.length > 1) {
      callback = query;
      query = undefined;
    }
  } else if (typeof query === 'number') {
    code = query;
    query = undefined;
  }
  //Set defaults if not exist
  return {
    query: query || {},
    callback: callback || callbackStub,
    code: code || 200
  };
};

function parseRawResponse(res, callback) {
  res.setEncoding('binary');
  res.data = '';
  res.on('data', function (chunk) {
    res.data += chunk;
  });
  res.on('end', function () {
    callback(null, res.data);
  });
}

Caller.prototype.asJson = function (enable) {
  return this._flag('jsonFormat', enable);
};
Caller.prototype.continueOnError = function (enable) {
  return this._flag('errorContinuation', enable);
};

Caller.prototype._call = function (method, params) {
  var $ = this;
  return function (value, done) {
    if (!done) {
      done = value;
      value = undefined;
    }
    params.value = value;
    params.done = done;
    $._request(method, params, function (error, response, body) {
      if (!$.errorContinuation && error) {
        throw error;
      }
      params.callback({
        error: error,
        response: response,
        body: body,
        value: value
      }, done);
    });
  };
};

Caller.prototype.get = function (query, callback, code) {
  var $ = this;
  var params = $.prepareParams(query, callback, code);
  $.calls.push($._call('GET', params));
  return $;
};
Caller.prototype.post = function (query, callback, code) {
  var $ = this;
  var params = $.prepareParams(query, callback, code);
  $.calls.push($._call('POST', params));
  return $;
};
Caller.prototype.multipart = function (query, callback, code) {
  var $ = this;
  var params = $.prepareParams(query, callback, code);
  params.multipart = true;
  $.calls.push($._call('POST', params));
  return $;
};
Caller.prototype.del = function (query, callback, code) {
  var $ = this;
  var params = $.prepareParams(query, callback, code);
  $.calls.push($._call('DELETE', params));
  return $;
};
Caller.prototype.put = function (query, callback, code) {
  var $ = this;
  var params = $.prepareParams(query, callback, code);
  $.calls.push($._call('PUT', params));
  return $;
};

Caller.prototype._reset = function () {
  var $ = this;
  $.subPath = '';
  $.headersObject = {};
  $.debugging = false;
  $.rawResponse = false;
};

Caller.prototype.run = function (done) {
  var $ = this;
  $._reset();
  if ($.calls.length > 0) {
    async.waterfall($.calls, function (error, arg) {
      $.calls = [];
      done(error, arg);
    });
  } else {
    done(new Error('Nothing to run. Call is redundant.'));
  }
  return $;
};
Caller.prototype.req = function (method, body, callback, code) {
  switch (method.toUpperCase()) {
    case 'GET':
      return this.get(body, callback, code);
    case 'POST':
      return this.post(body, callback, code);
    case 'PUT':
      return this.put(body, callback, code);
    case 'DELETE':
      return this.del(body, callback, code);
  }
};

module.exports = Caller;
